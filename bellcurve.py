from random import randint
import signal

def handler(signum, frame):
  print()
  print("Program interrupted")
  exit(0)

signal.signal(signal.SIGINT, handler)

def createBellCurve(faces, dicecount, rollcount):
  bins = [0] * ((faces - 1) * dicecount + 1)
  for x in range(rollcount):
    roll = 0
    for x in range(dicecount):
      roll += randint(1, faces)
    try:
      bins[roll - dicecount] += 1
    except:
      print(f'Array len when error happened {len(bins)}')
      quit()

  return bins

def printBellCurve(bins, count):
  for i in bins:
    partialblock = ["", "▏", "▎", "▍", "▌", "▋", "▊", "▉"]
    fullblocks = int(i / 8)
    remainder = int(i) % 8
    bar =  "█" * fullblocks + partialblock[remainder]
    print(f"{count:3d}: {bar}")
    count += 1
  print()

faces = 6
dicecount = 4
rollcount = 10000

while True:
  faces = int(input(f"Noppien sivujen määrä ({faces}): ") or faces)
  dicecount = int(input(f"Noppien lukumäärä ({dicecount}): ") or dicecount)
  rollcount = int(input(f"Kuinka monta kertaa heitetään ({rollcount}): ") or rollcount)
  bins = createBellCurve(faces,dicecount,rollcount)
  printBellCurve(bins, dicecount)
